# Computer Science Educational Program
## IT Operations Design

This is a repository outlining what we have found to be successful in terms of creating a successful Computer Science Education Program in a High School Setting.

We share these tools and documents in the hope that other Computer Science Education Programs will find them useful.

### Documents

(available in the [Docs Directory](docs))

- CS Program: 1-to-1 Laptop Program Design
- CS Program: Educational Space Design
- CS Program: Laptop Contract Template
- CS Program: IT Ops for a Program Development HQ + Deployment Format (Scaling an Existing Program) 


### Tools
(available in the [Parent List of Repositories](https://bitbucket.org/App_IT))

- SIMS - **S**imple **I**nventory **M**anagement **S**ystem *-------- in (PHP / MySQL)*
- MaCS - **Ma**ss **C**loning **S**ystem -----------------------------*with (Ubuntu / DRBL Clonezilla Server + Apache-based Web UI)*
- infoCap - **Info**rmation **Cap**ture Web App *--------------- in (PHP / MySQL)*
- theQuAD - **Qu**ality (Student) **A**ssignment **D**isplay *---- in (PHP / JavaScript)*

### License(s)
All tools and documents are free of cost and open-source under the MIT License.  Anyone is welcome to copy and modify the documents and tools or any part thereof.